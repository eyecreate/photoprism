FROM cloudron/base:3.2.0@sha256:ba1d566164a67c266782545ea9809dc611c4152e27686fd14060332dd88263ea

RUN mkdir -p /app/code
WORKDIR /app/code

ARG VERSION=220901-f493607b0

RUN apt-get update && apt-get install -y --no-install-recommends \
        wget \
        ca-certificates \
        sqlite3 \
        tzdata \
        libheif-examples \
        gnupg \
        gpg-agent \
        apt-utils \
        add-apt-key \
        exiftool \
        rawtherapee \
        ffmpeg \
        npm
        
RUN rm -rf /usr/local/go-1.17.5/* && wget https://golang.org/dl/go1.19.1.linux-amd64.tar.gz && tar -C /usr/local/go-1.17.5 --strip=1 -xzf go1.19.1.linux-amd64.tar.gz && rm -rf wget go1.19.1.linux-amd64.tar.gz
RUN wget https://dl.photoprism.org/tensorflow/linux/libtensorflow-linux-avx2-1.15.2.tar.gz && tar -C /usr/local -xzf libtensorflow-linux-avx2-1.15.2.tar.gz && ldconfig && rm libtensorflow-linux-avx2-1.15.2.tar.gz
RUN git clone https://github.com/photoprism/photoprism.git photoprism-build && cd photoprism-build && git checkout ${VERSION} && make all && ./scripts/build.sh prod /app/code && cp -a assets /app/code/assets && rm -rf /app/code/photoprism-build 

ENV PHOTOPRISM_STORAGE_PATH="/app/data/photoprism"
ENV PHOTOPRISM_ORIGINALS_PATH="/app/data/photoprism/photos/Originals"
ENV PHOTOPRISM_IMPORT_PATH="/app/data/photoprism/photos/Import"

RUN chmod a+r /app/code/assets/nasnet/labels.txt

COPY start.sh /app/pkg/

CMD [ "/app/pkg/start.sh" ]

