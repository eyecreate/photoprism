#!/bin/bash

export PHOTOPRISM_DATABASE_DRIVER="sqlite"
#export PHOTOPRISM_DATABASE_SERVER="${CLOUDRON_MYSQL_HOST}"
#export PHOTOPRISM_DATABASE_NAME="${CLOUDRON_MYSQL_DATABASE}"
#export PHOTOPRISM_DATABASE_USER="${CLOUDRON_MYSQL_USERNAME}"
#export PHOTOPRISM_DATABASE_PASSWORD="${CLOUDRON_MYSQL_PASSWORD}" 
export PHOTOPRISM_AUTH_MODE="password"
export PHOTOPRISM_ADMIN_PASSWORD="photoprism"
export PHOTOPRISM_HTTP_PORT=3000

if [ ! -f /app/data/photoprism ]; then
    mkdir -p /app/data/photoprism/photos/Originals
    mkdir -p /app/data/photoprism/photos/Import
    chown -R cloudron:cloudron /app/data/photoprism
fi

cd /app/code
/usr/local/bin/gosu cloudron:cloudron /app/code/photoprism up
